# ExercisesApi

All URIs are relative to *http://localhost:8080*

Method | HTTP request | Description
------------- | ------------- | -------------
[**deleteExercise**](ExercisesApi.md#deleteExercise) | **DELETE** /api/exercises/{id} | Delete an exercise
[**findAllExercises**](ExercisesApi.md#findAllExercises) | **GET** /api/exercises | Find all exercises
[**findExerciseById**](ExercisesApi.md#findExerciseById) | **GET** /api/exercises/{id} | Info for a specific exercise
[**saveExercises**](ExercisesApi.md#saveExercises) | **POST** /api/exercises | Save an exercise


<a name="deleteExercise"></a>
# **deleteExercise**
> deleteExercise(id)

Delete an exercise

### Example
```java
// Import classes:
//import io.swagger.client.ApiException;
//import io.swagger.client.api.ExercisesApi;


ExercisesApi apiInstance = new ExercisesApi();
Integer id = 56; // Integer | The id of exercise to deleteUser
try {
    apiInstance.deleteExercise(id);
} catch (ApiException e) {
    System.err.println("Exception when calling ExercisesApi#deleteExercise");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **Integer**| The id of exercise to deleteUser |

### Return type

null (empty response body)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

<a name="findAllExercises"></a>
# **findAllExercises**
> Exercises findAllExercises()

Find all exercises

### Example
```java
// Import classes:
//import io.swagger.client.ApiException;
//import io.swagger.client.api.ExercisesApi;


ExercisesApi apiInstance = new ExercisesApi();
try {
    Exercises result = apiInstance.findAllExercises();
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling ExercisesApi#findAllExercises");
    e.printStackTrace();
}
```

### Parameters
This endpoint does not need any parameter.

### Return type

[**Exercises**](Exercises.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

<a name="findExerciseById"></a>
# **findExerciseById**
> Exercise findExerciseById(id)

Info for a specific exercise

### Example
```java
// Import classes:
//import io.swagger.client.ApiException;
//import io.swagger.client.api.ExercisesApi;


ExercisesApi apiInstance = new ExercisesApi();
Integer id = 56; // Integer | The id of exercise to retrieve
try {
    Exercise result = apiInstance.findExerciseById(id);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling ExercisesApi#findExerciseById");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **Integer**| The id of exercise to retrieve |

### Return type

[**Exercise**](Exercise.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

<a name="saveExercises"></a>
# **saveExercises**
> Exercise saveExercises(exercise)

Save an exercise

### Example
```java
// Import classes:
//import io.swagger.client.ApiException;
//import io.swagger.client.api.ExercisesApi;


ExercisesApi apiInstance = new ExercisesApi();
Exercise exercise = new Exercise(); // Exercise | Exercise to be saved
try {
    Exercise result = apiInstance.saveExercises(exercise);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling ExercisesApi#saveExercises");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **exercise** | [**Exercise**](Exercise.md)| Exercise to be saved |

### Return type

[**Exercise**](Exercise.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

