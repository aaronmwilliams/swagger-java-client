
# Log

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**id** | **Long** |  |  [optional]
**exerciseId** | **Integer** |  |  [optional]
**date** | **String** |  |  [optional]
**userId** | **Long** |  |  [optional]
**userName** | **String** |  |  [optional]
**description** | **String** |  |  [optional]



