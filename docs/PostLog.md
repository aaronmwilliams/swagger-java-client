
# PostLog

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**userId** | **Long** |  |  [optional]
**exerciseId** | **Integer** |  |  [optional]
**date** | **String** |  |  [optional]



