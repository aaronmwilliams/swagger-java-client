# LogApi

All URIs are relative to *http://localhost:8080*

Method | HTTP request | Description
------------- | ------------- | -------------
[**deleteLog**](LogApi.md#deleteLog) | **DELETE** /api/log/{id} | Delete a log
[**findLogById**](LogApi.md#findLogById) | **GET** /api/log/{id} | Info for a specific user
[**saveLog**](LogApi.md#saveLog) | **POST** /api/log | Save a log entry


<a name="deleteLog"></a>
# **deleteLog**
> deleteLog(id)

Delete a log

### Example
```java
// Import classes:
//import io.swagger.client.ApiException;
//import io.swagger.client.api.LogApi;


LogApi apiInstance = new LogApi();
Integer id = 56; // Integer | The id of exercise to deleteUser
try {
    apiInstance.deleteLog(id);
} catch (ApiException e) {
    System.err.println("Exception when calling LogApi#deleteLog");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **Integer**| The id of exercise to deleteUser |

### Return type

null (empty response body)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

<a name="findLogById"></a>
# **findLogById**
> Log findLogById(id)

Info for a specific user

### Example
```java
// Import classes:
//import io.swagger.client.ApiException;
//import io.swagger.client.api.LogApi;


LogApi apiInstance = new LogApi();
Integer id = 56; // Integer | The id of the user to retrieve
try {
    Log result = apiInstance.findLogById(id);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling LogApi#findLogById");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **Integer**| The id of the user to retrieve |

### Return type

[**Log**](Log.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

<a name="saveLog"></a>
# **saveLog**
> Log saveLog(log)

Save a log entry

### Example
```java
// Import classes:
//import io.swagger.client.ApiException;
//import io.swagger.client.api.LogApi;


LogApi apiInstance = new LogApi();
PostLog log = new PostLog(); // PostLog | Log to be saved
try {
    Log result = apiInstance.saveLog(log);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling LogApi#saveLog");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **log** | [**PostLog**](PostLog.md)| Log to be saved |

### Return type

[**Log**](Log.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

