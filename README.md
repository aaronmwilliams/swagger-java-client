# swagger-java-client

## Requirements

Building the API client library requires [Maven](https://maven.apache.org/) to be installed.

## Installation

To install the API client library to your local Maven repository, simply execute:

```shell
mvn install
```

To deploy it to a remote Maven repository instead, configure the settings of the repository and execute:

```shell
mvn deploy
```

Refer to the [official documentation](https://maven.apache.org/plugins/maven-deploy-plugin/usage.html) for more information.

### Maven users

Add this dependency to your project's POM:

```xml
<dependency>
    <groupId>io.swagger</groupId>
    <artifactId>swagger-java-client</artifactId>
    <version>1.0.0</version>
    <scope>compile</scope>
</dependency>
```

### Gradle users

Add this dependency to your project's build file:

```groovy
compile "io.swagger:swagger-java-client:1.0.0"
```

### Others

At first generate the JAR by executing:

    mvn package

Then manually install the following JARs:

* target/swagger-java-client-1.0.0.jar
* target/lib/*.jar

## Getting Started

Please follow the [installation](#installation) instruction and execute the following Java code:

```java

import io.swagger.client.*;
import io.swagger.client.auth.*;
import io.swagger.client.model.*;
import io.swagger.client.api.ExercisesApi;

import java.io.File;
import java.util.*;

public class ExercisesApiExample {

    public static void main(String[] args) {
        
        ExercisesApi apiInstance = new ExercisesApi();
        Integer id = 56; // Integer | The id of exercise to deleteUser
        try {
            apiInstance.deleteExercise(id);
        } catch (ApiException e) {
            System.err.println("Exception when calling ExercisesApi#deleteExercise");
            e.printStackTrace();
        }
    }
}

```

## Documentation for API Endpoints

All URIs are relative to *http://localhost:8080*

Class | Method | HTTP request | Description
------------ | ------------- | ------------- | -------------
*ExercisesApi* | [**deleteExercise**](docs/ExercisesApi.md#deleteExercise) | **DELETE** /api/exercises/{id} | Delete an exercise
*ExercisesApi* | [**findAllExercises**](docs/ExercisesApi.md#findAllExercises) | **GET** /api/exercises | Find all exercises
*ExercisesApi* | [**findExerciseById**](docs/ExercisesApi.md#findExerciseById) | **GET** /api/exercises/{id} | Info for a specific exercise
*ExercisesApi* | [**saveExercises**](docs/ExercisesApi.md#saveExercises) | **POST** /api/exercises | Save an exercise
*LogApi* | [**deleteLog**](docs/LogApi.md#deleteLog) | **DELETE** /api/log/{id} | Delete a log
*LogApi* | [**findLogById**](docs/LogApi.md#findLogById) | **GET** /api/log/{id} | Info for a specific user
*LogApi* | [**saveLog**](docs/LogApi.md#saveLog) | **POST** /api/log | Save a log entry
*UsersApi* | [**deleteUser**](docs/UsersApi.md#deleteUser) | **DELETE** /api/users/{id} | Delete a user
*UsersApi* | [**findAllUsers**](docs/UsersApi.md#findAllUsers) | **GET** /api/users | Find all users
*UsersApi* | [**findUserById**](docs/UsersApi.md#findUserById) | **GET** /api/users/{id} | Info for a specific user
*UsersApi* | [**saveUser**](docs/UsersApi.md#saveUser) | **POST** /api/users | Save a user


## Documentation for Models

 - [Exercise](docs/Exercise.md)
 - [Exercises](docs/Exercises.md)
 - [Log](docs/Log.md)
 - [PostLog](docs/PostLog.md)
 - [User](docs/User.md)
 - [Users](docs/Users.md)


## Documentation for Authorization

All endpoints do not require authorization.
Authentication schemes defined for the API:

## Recommendation

It's recommended to create an instance of `ApiClient` per thread in a multithreaded environment to avoid any potential issues.

## Author



