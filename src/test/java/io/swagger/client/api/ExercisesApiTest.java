/*
 * User Exercises Rest API
 * No description provided (generated by Swagger Codegen https://github.com/swagger-api/swagger-codegen)
 *
 * OpenAPI spec version: 1.0.0
 * 
 *
 * NOTE: This class is auto generated by the swagger code generator program.
 * https://github.com/swagger-api/swagger-codegen.git
 * Do not edit the class manually.
 */


package io.swagger.client.api;

import io.swagger.client.model.Exercise;
import io.swagger.client.model.Exercises;
import org.junit.Test;
import org.junit.Ignore;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * API tests for ExercisesApi
 */
@Ignore
public class ExercisesApiTest {

    private final ExercisesApi api = new ExercisesApi();

    
    /**
     * Delete an exercise
     *
     * 
     *
     * @throws ApiException
     *          if the Api call fails
     */
    @Test
    public void deleteExerciseTest() {
        Integer id = null;
        api.deleteExercise(id);

        // TODO: test validations
    }
    
    /**
     * Find all exercises
     *
     * 
     *
     * @throws ApiException
     *          if the Api call fails
     */
    @Test
    public void findAllExercisesTest() {
        Exercises response = api.findAllExercises();

        // TODO: test validations
    }
    
    /**
     * Info for a specific exercise
     *
     * 
     *
     * @throws ApiException
     *          if the Api call fails
     */
    @Test
    public void findExerciseByIdTest() {
        Integer id = null;
        Exercise response = api.findExerciseById(id);

        // TODO: test validations
    }
    
    /**
     * Save an exercise
     *
     * 
     *
     * @throws ApiException
     *          if the Api call fails
     */
    @Test
    public void saveExercisesTest() {
        Exercise exercise = null;
        Exercise response = api.saveExercises(exercise);

        // TODO: test validations
    }
    
}
